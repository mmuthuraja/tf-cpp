Research on predicting using tf-cpp

# Development environment
Just a small warning: this step takes time...
```sh
docker build -t tf_cpp -f dev.dockerfile .
```

# Building the project
```sh
docker run -e HOME=/tmp/home --rm --network host --mount type=bind,source=$(pwd),target=/tmp/project,readonly -w=/tmp/project -it tf_cpp /bin/bash
```

Using this docker command prevents downloading and building all conan packages all the time:
```sh
docker run --rm --mount type=bind,source=$(pwd)/.conan,target=/tmp/home -e HOME=/tmp/home --mount type=bind,source=$(pwd),target=/tmp/project,readonly -w=/tmp/build -it tf_cpp /bin/bash
```

```sh
mkdir -p /tmp/build
cd /tmp/build
cmake ../project
cmake --build .
```

# Futher project configuration
If you want to configure the project you can use ccmake
```sh
ccmake .
```
This allows to create sanitizer builds, disable warnings-as-errors, coverage, and more.

# Running the tests
```sh
ctest
```

# Additional targets

```sh
cmake --build . --target complexity
cmake --build . --target statistics
```

# Code formating
Clang-format can be used to check and do code formating. Make sure not to include cmake files in clang-format inline edit command.
```sh
clang-format -style=file -i **/*.h **/*.hpp **/*.c **/*.cpp
```

# Sanitizer Tests
Due to our TDD approach, the following sanitizer checks are required {ADDRESS, LEAK, THREAD, UNDEFINED_BEH}. On the basic level, a sanitizer is a memory detector during run-time. Given our constraints the address sanitizer looks for errors in the memory, the leak sanitizer keeps track of memory leaks, and so on. Not all sanitizers can be run in parallel, however, make sure to execute as many sanitizers as possible in parallel due to performance constants. In general, sanitizers a very slow in comparison to a build stage. Consequently, you need to run two independent sanitizer checks to cover the requirements. In the first check activate {ADDRESS, LEAK, UNDEFINED_BEH} and in the second check run the {THREAD} sanitizer only.
