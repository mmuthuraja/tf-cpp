#pragma once

#include "../src/TensorflowUtilities.cpp"
#include <gtest/gtest.h>

TEST(TensorflowUtilitiesTest, tf_import_check) {
  // Test has no assert as the only way it fails is by not compiling
  bool status = rbc::TensorflowUtilities::tensor_flow_build_status();
  ASSERT_TRUE(status);
}

TEST(TensorflowUtilitiesTest, mask_rcnn_predict) {
  cv::Mat input_image = cv::imread("/tmp/project/test/stitched_img.png");
  cv::Mat input_image_rgb;
  cv::cvtColor(input_image, input_image_rgb, cv::COLOR_BGR2RGB);
  cv::Mat dummy = rbc::TensorflowUtilities::predict(input_image_rgb);
  cv::imwrite("/tmp/project/test/dummy.png", dummy);
}