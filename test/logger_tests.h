#pragma once

#include "../src/logger.h"
#include <gtest/gtest.h>

TEST(LoggerTest, Can_call_set_log_level) {
  // Test has no assert as the only way it fails is by not compiling
  rbc::Logger::set_log_level(rbc::LogLevel::trace);
}

TEST(LoggerTest, Can_call_trace) {
  // Test has no assert as the only way it fails is by not compiling
  rbc::Logger::trace("Caller", "message");
}

TEST(LoggerTest, Can_call_debug) {
  // Test has no assert as the only way it fails is by not compiling
  rbc::Logger::debug("Caller", "message");
}

TEST(LoggerTest, Can_call_info) {
  // Test has no assert as the only way it fails is by not compiling
  rbc::Logger::info("Caller", "message");
}

TEST(LoggerTest, Can_call_warning) {
  // Test has no assert as the only way it fails is by not compiling
  rbc::Logger::warning("Caller", "message");
}

TEST(LoggerTest, Can_call_error) {
  // Test has no assert as the only way it fails is by not compiling
  rbc::Logger::error("Caller", "message");
}

TEST(LoggerTest, Can_call_critical) {
  // Test has no assert as the only way it fails is by not compiling
  rbc::Logger::critical("Caller", "message");
}
