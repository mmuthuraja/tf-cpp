find_package(OpenCV REQUIRED core imgproc imgcodecs calib3d)
find_package(Eigen3 3.3 REQUIRED NO_MODULE)

include_directories(${OpenCV_INCLUDE_DIRS})
include(GoogleTest)


add_executable(gtest gtest.cpp
)
target_link_libraries(gtest PRIVATE 
  project_warnings
  project_options
  CONAN_PKG::gtest
  ${PROJECT_NAME}-lib
)
target_include_directories(gtest PRIVATE ${CMAKE_SOURCE_DIR}/include)

gtest_discover_tests(
  gtest
  TEST_PREFIX
  "gtest."
  EXTRA_ARGS
  --gtest_output=xml:gtest.xml
  DISCOVERY_TIMEOUT 10000
)
