#include "../src/logger.h"
#include <gtest/gtest.h>

#include "TensorflowUtilities_test.h"
#include "logger_tests.h"
#include <Eigen/Eigen>

int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  rbc::Logger::set_log_level(rbc::LogLevel::trace);
  return RUN_ALL_TESTS();
}