FROM debian:buster-slim as llvmbase
ARG LLVM_VERSION=11
ENV DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Berlin
RUN apt-get -qq update \
    && apt-get -qq upgrade -y \
    && apt-get -qq install -y software-properties-common gnupg \
    && apt-get -qq install -y libncurses6 libcurl4-openssl-dev openssl\
    && apt-get -qq install -y wget curl \
    && wget https://apt.llvm.org/llvm.sh \
    && chmod +x llvm.sh \
    && ./llvm.sh ${LLVM_VERSION} \
    && rm llvm.sh \
    && apt-get -qq update \
    && apt-get -qq install -y clang-format-11 clang-tidy-11 clang-tools-11 \
    && rm -rf /var/lib/apt/lists/* \
    && update-alternatives --install /usr/bin/cc cc /usr/bin/clang-${LLVM_VERSION} 100 \
    && update-alternatives --install /usr/bin/c++ c++ /usr/bin/clang++-${LLVM_VERSION} 100 \
    && update-alternatives --install /usr/bin/ld ld /usr/bin/lld-${LLVM_VERSION} 100 \
    && update-alternatives --config cc \
    && update-alternatives --config c++ \
    && update-alternatives --config ld \
    && rm -f /usr/bin/clang \
    && rm -f /usr/bin/clang++ \
    && ln -s /usr/bin/clang-${LLVM_VERSION} /usr/bin/clang \
    && ln -s /usr/bin/clang++-${LLVM_VERSION} /usr/bin/clang++ \
    && ln -s /usr/bin/clang-format-${LLVM_VERSION} /usr/bin/clang-format \
    && ln -s /usr/bin/lld-${LLVM_VERSION} /usr/bin/lld \
    && ln -s /usr/bin/clang-tidy-${LLVM_VERSION} /usr/bin/clang-tidy

FROM debian:buster-slim as DebianPackages
ARG CONAN_VERSION=1_33_0
RUN mkdir -p /var/debs/
RUN apt-get -q -q update \
    && apt-get -q -q install -y wget \
    && cd /var/debs/ \
    && wget https://dl.bintray.com/conan/installers/conan-ubuntu-64_${CONAN_VERSION}.deb

FROM llvmbase as GitBuild
ARG GIT_VERSION=2.29.2
RUN apt-get -q -q update \
    && apt-get -q -q install -y git autoconf make curl zlib1g zlib1g-dev expat install-info gettext
RUN cd /tmp/ \
    && git clone https://github.com/git/git.git --branch="v${GIT_VERSION}"
RUN cd /tmp/git \
    && make configure \
    && ./configure --prefix=/usr/local \
    && make -j16 \
    && make install


FROM llvmbase as NinjaBuild
ARG NINJA_VERSION=1.10.1
COPY --from=GitBuild /usr/local/ /usr/local/
RUN apt-get -q -q update \
    && apt-get -q -q install -y python3 \
    && ln -s /usr/bin/python3 /usr/bin/python
RUN mkdir -p /tmp/ && cd /tmp/ \
    && git clone https://github.com/ninja-build/ninja.git --branch="v${NINJA_VERSION}"
RUN cd /tmp/ninja/ \
    && CXX=clang++ ./configure.py --bootstrap


FROM llvmbase as CMakeBuild
ARG CMAKE_VERSION=3.18.4
COPY --from=GitBuild /usr/local/ /usr/local/
RUN apt-get -q -q update \
    && apt-get -q -q install -y make libssl-dev
RUN mkdir -p /tmp/cmake-prebuild/ \
    && cd /tmp/ \
    && git clone https://gitlab.kitware.com/cmake/cmake.git --branch="v${CMAKE_VERSION}"
RUN cd /tmp/cmake \
    && ./bootstrap --prefix=/tmp/cmake-prebuild \
    && make -j16 \
    && make install


FROM llvmbase as CcacheBuild
ARG CCACHE_VERSION=4.0
COPY --from=GitBuild /usr/local/ /usr/local/
RUN apt-get -qq update \
    && apt-get -qq install -y cmake make
RUN mkdir -p /tmp/ccache-prebuild/ \
    && cd /tmp/ \
    && git clone https://github.com/ccache/ccache.git --branch="v${CCACHE_VERSION}"
RUN cd /tmp/ccache \
    && mkdir -p build \
    && cd build \
    && cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_PREFIX_PATH=/tmp/ccache-prebuild -DZSTD_FROM_INTERNET=ON .. \
    && make -j16 \
    && make install

FROM llvmbase as DevBase
COPY --from=DebianPackages /var/debs/ /tmp/debs/
COPY --from=GitBuild /usr/local/ /usr/local/
COPY --from=NinjaBuild /tmp/ninja/ninja /usr/local/bin/
COPY --from=CMakeBuild /tmp/cmake-prebuild/ /usr/local/
COPY --from=CcacheBuild /tmp/ccache-prebuild/ /usr/local/

RUN apt-get -qq update \
    && apt-get -qq install -y syslog-ng \
    && apt-get -qq install -y python3 python3-pip vera++ cppcheck cloc iwyu /tmp/debs/* \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /tmp/debs 
RUN pip3 install lizard \
    && pip3 install cmake_format
RUN mkdir -p /tmp/build

FROM devbase
ENV DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Berlin

RUN apt-get -q -q update \
    && apt-get -q -q install -y \
        sudo \
        libpciaccess-dev \
        libgtk2.0-dev \
        unzip \
        zip \
        net-tools \
        iproute2 \
        iputils-ping \
        vim

RUN mkdir -p tmp/deps/opencv \
    && cd tmp/deps/opencv \
    && wget -O opencv.zip https://github.com/opencv/opencv/archive/refs/tags/4.5.2.zip \
    && unzip opencv.zip \
    && rm opencv.zip \
    && cd ./opencv-4.5.2 \
    && mkdir -p build && cd build \
    && cmake -DCMAKE_BUILD_TYPE=Release .. \
    && make -j16 install

RUN apt-get -q -q update \
    && apt-get -q -q install -y \
        libeigen3-dev 


# Tensorflow Installations
# FROM python:3.8.6-slim-buster


# install system dependencies
RUN DEBIAN_FRONTEND=noninteractive apt-get update \
    && apt-get install -y \
        build-essential \
        python3-pip \
        libsm6 \
        libxext6 \
        libxrender-dev \
        python3-opencv \
	wget \
	curl \
	vim \
    python3-dev \
    python3-numpy \
    curl \
    g++-7 \
    && rm -rf /var/lib/apt/lists/*

# cuda installations
RUN apt-get update && apt-get install -y --no-install-recommends \
    gnupg2 curl ca-certificates && \
    curl -fsSL https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/7fa2af80.pub | apt-key add - && \
    echo "deb https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64 /" > /etc/apt/sources.list.d/cuda.list && \
    echo "deb https://developer.download.nvidia.com/compute/machine-learning/repos/ubuntu1804/x86_64 /" > /etc/apt/sources.list.d/nvidia-ml.list && \
    apt-get purge --autoremove -y curl \
    && rm -rf /var/lib/apt/lists/*

ENV CUDA_VERSION 10.1.243
ENV CUDA_PKG_VERSION 10-1=$CUDA_VERSION-1

# For libraries in the cuda-compat-* package: https://docs.nvidia.com/cuda/eula/index.html#attachment-a
RUN apt-get update && apt-get install -y --no-install-recommends \
    cuda-cudart-$CUDA_PKG_VERSION \
    cuda-compat-10-1 \
    && ln -s cuda-10.1 /usr/local/cuda && \
    rm -rf /var/lib/apt/lists/*

# Required for nvidia-docker v1
RUN echo "/usr/local/nvidia/lib" >> /etc/ld.so.conf.d/nvidia.conf && \
    echo "/usr/local/nvidia/lib64" >> /etc/ld.so.conf.d/nvidia.conf

ENV PATH /usr/local/nvidia/bin:/usr/local/cuda/bin:${PATH}
ENV LD_LIBRARY_PATH /usr/local/nvidia/lib:/usr/local/nvidia/lib64

# nvidia-container-runtime
ENV NVIDIA_VISIBLE_DEVICES all
ENV NVIDIA_DRIVER_CAPABILITIES compute,utility
ENV NVIDIA_REQUIRE_CUDA "cuda>=10.1 brand=tesla,driver>=396,driver<397 brand=tesla,driver>=410,driver<411 brand=tesla,driver>=418,driver<419"

ENV NCCL_VERSION 2.7.8

RUN apt-get update && apt-get install -y --no-install-recommends \
    cuda-libraries-$CUDA_PKG_VERSION \
    cuda-npp-$CUDA_PKG_VERSION \
    cuda-nvtx-$CUDA_PKG_VERSION \
    libcublas10=10.2.1.243-1 \
    libnccl2=$NCCL_VERSION-1+cuda10.1 \
    && apt-mark hold libnccl2 \
    && rm -rf /var/lib/apt/lists/*

# apt from auto upgrading the cublas package. See https://gitlab.com/nvidia/container-images/cuda/-/issues/88
RUN apt-mark hold libcublas10

ENV CUDNN_VERSION 7.6.5.32

LABEL com.nvidia.cudnn.version="${CUDNN_VERSION}"

RUN apt-get update && apt-get install -y --no-install-recommends \
    libcudnn7=$CUDNN_VERSION-1+cuda10.1 \
    && apt-mark hold libcudnn7 && \
    rm -rf /var/lib/apt/lists/*


# Add Tini. Tini operates as a process subreaper for jupyter. This prevents kernel crashes.
ENV TINI_VERSION v0.6.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /usr/bin/tini
RUN chmod +x /usr/bin/tini
ENTRYPOINT ["/usr/bin/tini", "--"]
EXPOSE 8888
    
ENV PATH /usr/bin/pip:$PATH
ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8

WORKDIR /home
ENV PYTHONPATH=/usr/bin/python3.8

# Install Tensorflow CC
# Ref: https://github.com/FloopCZ/tensorflow_cc/
RUN apt-get update && apt-get install -y --no-install-recommends \
    curl git && \
    curl -fsSL https://bazel.build/bazel-release.pub.gpg | gpg --dearmor > bazel.gpg
RUN mv bazel.gpg /etc/apt/trusted.gpg.d/
RUN echo "deb [arch=amd64] https://storage.googleapis.com/bazel-apt stable jdk1.8" | tee /etc/apt/sources.list.d/bazel.list
RUN apt-get update &&  apt-get install -y --no-install-recommends \
    bazel \
    bazel-3.7.2

RUN apt-get update &&  apt-get install -y --no-install-recommends \
    python3-dev \
    python3-numpy \
    python3-pip  \
    git && \
    pip3 install numpy && \
    git clone https://github.com/FloopCZ/tensorflow_cc.git && cd tensorflow_cc && cd tensorflow_cc &&  mkdir build && cd build && cmake .. && make && make install && ldconfig