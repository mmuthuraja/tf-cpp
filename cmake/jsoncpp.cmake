cmake_minimum_required(VERSION 3.5)
include(FetchContent)

FetchContent_Declare(
  jsoncpp
  GIT_REPOSITORY https://github.com/open-source-parsers/jsoncpp
  GIT_TAG        master
)

FetchContent_MakeAvailable(jsoncpp)

FetchContent_GetProperties(jsoncpp)
if(NOT jsoncpp_POPULATED)
  FetchContent_Populate(jsoncpp)
  add_subdirectory(${jsoncpp_SOURCE_DIR} ${jsoncpp_BINARY_DIR})
endif()

set(jsoncpp_INCLUDE_DIRS ${jsoncpp_SOURCE_DIR}/include)
set(jsoncpp_LIBS ${CMAKE_BINARY_DIR}/lib/libjsoncpp.a)