option(ENABLE_MUTATION_TESTING "Enable mutation tests" OFF)

function(enable_mutation_testing project_options)
    if(ENABLE_MUTATION_TESTING)
        find_program(MULL mull-cxx)
        if(MULL)
            set(CMAKE_BUILD_TYPE Debug)
            set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
            target_compile_options(project_options INTERFACE -g -O0 -fembed-bitcode)
            


        else()
            message(SEND_ERROR "Could not find mutation testing framework mull")
        endif()
    endif()
endfunction()
