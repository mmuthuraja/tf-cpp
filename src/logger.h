#pragma once

#include <string>

namespace rbc {

enum class LogLevel {
  trace = 0,
  debug = 1,
  info = 2,
  warning = 3,
  error = 4,
  critical = 5,
  off = 6
};

class Logger final {
public:
  static void set_log_level(const LogLevel &level) noexcept;
  static void trace(const std::string &caller,
                    const std::string &message) noexcept;
  static void debug(const std::string &caller,
                    const std::string &message) noexcept;
  static void info(const std::string &caller,
                   const std::string &message) noexcept;
  static void warning(const std::string &caller,
                      const std::string &message) noexcept;
  static void error(const std::string &caller,
                    const std::string &message) noexcept;
  static void critical(const std::string &caller,
                       const std::string &message) noexcept;
};
} // namespace rbc
