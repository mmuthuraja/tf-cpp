#include "logger.h"

#include "spdlog/sinks/stdout_color_sinks.h"
#include <memory>
#include <mutex>
#include <spdlog/spdlog.h>

namespace rbc {

inline static std::mutex s_lock;
inline static const std::string LOG_PATTERN{"%^[%Y-%m-%d %T.%F%z] %n: %v%$"};
inline static std::shared_ptr<spdlog::logger> s_logger_ptr(nullptr);

void instanciate_logger_ptr() {
  std::lock_guard<std::mutex> guard(s_lock);
  if (nullptr == s_logger_ptr) {
    s_logger_ptr = spdlog::stdout_color_mt("RoBoCut");
    s_logger_ptr->set_pattern(LOG_PATTERN);
    s_logger_ptr->set_level(spdlog::level::level_enum::info);
  }
}

void Logger::set_log_level(const LogLevel &level) noexcept {
  auto spdLogLevel = static_cast<spdlog::level::level_enum>(level);
  instanciate_logger_ptr();
  s_logger_ptr->set_level(spdLogLevel);
}

void Logger::trace(const std::string &caller,
                   const std::string &message) noexcept {
  instanciate_logger_ptr();
  s_logger_ptr->trace(caller + " " + message);
}

void Logger::debug(const std::string &caller,
                   const std::string &message) noexcept {
  instanciate_logger_ptr();
  s_logger_ptr->debug(caller + " " + message);
}

void Logger::info(const std::string &caller,
                  const std::string &message) noexcept {
  instanciate_logger_ptr();
  s_logger_ptr->info(caller + " " + message);
}

void Logger::warning(const std::string &caller,
                     const std::string &message) noexcept {
  instanciate_logger_ptr();
  s_logger_ptr->warn(caller + " " + message);
}

void Logger::error(const std::string &caller,
                   const std::string &message) noexcept {
  instanciate_logger_ptr();
  s_logger_ptr->error(caller + " " + message);
}

void Logger::critical(const std::string &caller,
                      const std::string &message) noexcept {
  instanciate_logger_ptr();
  s_logger_ptr->critical(caller + " " + message);
}

} // namespace rbc