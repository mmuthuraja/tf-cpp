#include "TensorflowUtilities.h"

namespace rbc {

bool TensorflowUtilities::tensor_flow_build_status() {
  Session *session;
  Status status = NewSession(SessionOptions(), &session);
  if (!status.ok()) {
    cout << status.ToString() << "\n";
    return false;
  }
  return true;
}

tensorflow::Tensor TensorflowUtilities::get_anchors() {
  int inputImg_w = 1024;     //输入图像的宽度 image width
  int inputImg_h = 1024;     //输入图像的高度 image height
  int backbone_shape[5][2];  // for backbone_shape
  int _anchor_cache[2] = {}; //用于缓存 for cache
  int RPN_ANCHOR_SCALES[5] = {32, 64, 128, 256,
                              512};         // rpn阶段生成的anchor的尺度
  float RPN_ANCHOR_RATIOS[3] = {0.5, 1, 2}; // rpn阶段生成的anchor的缩放因子
  float BACKBONE_STRIDES[5] = {
      4, 8, 16, 32, 64}; //用于计算输入图像经过backbone的每一个阶段(可能是pooling或者conv等down
                         //sample操作导致feature
                         //map缩小后的尺寸,这一部分没细看)后feature图的长宽
  int RPN_ANCHOR_STRIDE = 1; // rpn阶段生成的anchor之间的间隔
  //下面是记录数组对应的个数,貌似可用sizeof来计算.....算了不想每次都计算,总觉得以后会用到...
  int rpn_anchor_scales_num = 5;
  int backbone_strides_num = 5;
  int rpn_anchor_ratios_num = 3;
  int batch_size = 1; // batch size

  Eigen::MatrixXf finalBox;
  Eigen::MatrixXf finalBox_norm;
  Eigen::MatrixXf finalboxMat;
  tensorflow::Tensor resized_tensor;      //输入的tensor
  tensorflow::Tensor inputMetadataTensor; //图像元数据
  tensorflow::Tensor inputAnchorsTensor;  //图像anchors数据

  if (inputImg_h != _anchor_cache[0] ||
      inputImg_w !=
          _anchor_cache[1]) //如果之前计算过就不用重新计算了,相当于cache
  {
    std::cout << "here" << std::endl;
    for (int i = 0; i < backbone_strides_num; i++) {
      backbone_shape[i][0] = static_cast<int>(
          ceil(inputImg_h / static_cast<int>(BACKBONE_STRIDES[i])));
      backbone_shape[i][1] = static_cast<int>(
          ceil(inputImg_w / static_cast<int>(BACKBONE_STRIDES[i])));
    }
    // std::vector<tensorflow::Tensor> anchors;
    std::vector<Eigen::MatrixXf> boxesVec; // eigen矩阵类型容器,用于存储anchors

    int finalBoxesRows =
        0; //用于统计五个RPN_ANCHOR_SCALES尺度对应的所有boxes的行数,可以先不看这个

    // generate_pyramid_anchors //生成不同尺度(配置参数中是5个)的anchor
    for (int j = 0; j < rpn_anchor_scales_num; j++) {
      // generate_anchors

      // Get all combinations of scales and ratios
      Eigen::RowVectorXf scalesVec(
          1); //遍历并且临时存储RPN_ANCHOR_SCALES[5]={32, 64, 128, 256,
              //512}的每个元素,主要给scalesMat赋值用
      Eigen::VectorXf ratiosVec(rpn_anchor_ratios_num);
      Eigen::MatrixXf scalesMat =
          Eigen::MatrixXf(rpn_anchor_ratios_num, 1); //();
      Eigen::MatrixXf ratiosMat =
          Eigen::MatrixXf(rpn_anchor_ratios_num, 1); //();
      Eigen::MatrixXf
          heightsMat; //=Eigen::MatrixXf(rpn_anchor_ratios_num, 1);//();
      Eigen::MatrixXf
          widthsMat; //=Eigen::MatrixXf(rpn_anchor_ratios_num, 1);//();
      scalesVec(0) = (static_cast<float>(RPN_ANCHOR_SCALES[j]));

      //构造np.array(ratios)
      for (int i = 0; i < rpn_anchor_ratios_num; i++) {
        ratiosVec(i) = RPN_ANCHOR_RATIOS[i];
      }
      for (int i = 0; i < ratiosMat.cols(); i++) {
        ratiosMat.col(i) << ratiosVec;
      }

      //构造np.array(scales)
      // std::cout<<"scalesMat is <<"<<scalesMat.cols()<<std::endl;
      for (int i = 0; i < scalesMat.rows(); i++) {
        scalesMat.row(i) << scalesVec;
      }

      // Enumerate heights and widths from scales and ratios
      heightsMat = scalesMat.cwiseQuotient(ratiosMat.cwiseSqrt());
      widthsMat = scalesMat.cwiseProduct(ratiosMat.cwiseSqrt());

      int step = RPN_ANCHOR_STRIDE, low = 0, hight_y = backbone_shape[j][0],
          hight_x = backbone_shape[j][1]; //获取shape[0],shape[1],anchor_stride,
      Eigen::RowVectorXf shifts_y;        //行向量
      Eigen::RowVectorXf shifts_x;
      int realsize_y = ((hight_y - low) / step);
      int realsize_x = ((hight_x - low) / step);
      shifts_y.setLinSpaced(realsize_y, static_cast<float>(low),
                            static_cast<float>(low + step * (realsize_y - 1)));
      shifts_x.setLinSpaced(realsize_x, static_cast<float>(low),
                            static_cast<float>(low + step * (realsize_x - 1)));
      shifts_y *= BACKBONE_STRIDES
          [j]; //获取feature_stride,这里的feature_stride其实是python代码中外围循环送进的参数BACKBONE_STRIDES[j]
      shifts_x *= BACKBONE_STRIDES
          [j]; //获取feature_stride,这里的feature_stride其实是python代码中外围循环送进的参数BACKBONE_STRIDES[j]

      /*再进行   shifts_x, shifts_y = np.meshgrid(shifts_x, shifts_y),
      构造出最终的shifts_x,shifts_y矩阵,注意经过np.meshgrid后shifts_x,shifts_y是二维的矩阵
      */
      //构造shifts_x,shifts_y矩阵
      Eigen::MatrixXf shifts_xMat(shifts_y.cols(), shifts_x.cols()),
          shifts_yMat(shifts_y.cols(), shifts_x.cols());
      for (int i = 0; i < shifts_xMat.rows(); i++) {
        shifts_xMat.row(i) = shifts_x;
      }
      for (int i = 0; i < shifts_yMat.cols(); i++) {
        shifts_yMat.col(i) = shifts_y;
      }

      Eigen::RowVectorXf heightsMatFlat(Eigen::Map<Eigen::VectorXf>(
          heightsMat.data(), heightsMat.rows() * heightsMat.cols()));
      Eigen::RowVectorXf widthsMatFlat(Eigen::Map<Eigen::VectorXf>(
          widthsMat.data(), widthsMat.rows() * widthsMat.cols()));

      shifts_xMat.transposeInPlace();
      shifts_yMat.transposeInPlace();
      Eigen::RowVectorXf shifts_yMatFlat(Eigen::Map<Eigen::VectorXf>(
          shifts_yMat.data(), shifts_yMat.rows() * shifts_yMat.cols()));
      // Eigen::RowVectorXf
      // shifts_xMatFlat(Eigen::Map<Eigen::VectorXf>(shifts_xMat.data(),shifts_xMat.rows()*shifts_xMat.cols(),Eigen::ColMajor));
      Eigen::RowVectorXf shifts_xMatFlat(Eigen::Map<Eigen::VectorXf>(
          shifts_xMat.data(), shifts_xMat.rows() * shifts_xMat.cols()));
      Eigen::MatrixXf box_widthsMat =
          Eigen::MatrixXf(shifts_xMatFlat.cols(), widthsMatFlat.cols()); //();
      Eigen::MatrixXf box_center_xMat =
          Eigen::MatrixXf(shifts_xMatFlat.cols(), widthsMatFlat.cols()); //();
      Eigen::MatrixXf box_heightsMat =
          Eigen::MatrixXf(shifts_yMatFlat.cols(), heightsMatFlat.cols()); //();
      Eigen::MatrixXf box_center_yMat =
          Eigen::MatrixXf(shifts_yMatFlat.cols(), heightsMatFlat.cols()); //();
      for (int i = 0; i < box_widthsMat.rows(); i++) {
        box_widthsMat.row(i) = widthsMatFlat;
        box_heightsMat.row(i) = heightsMatFlat;
      }
      for (int i = 0; i < box_heightsMat.cols(); i++) {
        box_center_xMat.col(i) = shifts_xMatFlat;
        box_center_yMat.col(i) = shifts_yMatFlat;
      }

      Eigen::MatrixXf y1Mat = box_center_yMat - box_heightsMat * 0.5;
      Eigen::MatrixXf x1Mat = box_center_xMat - box_widthsMat * 0.5;
      Eigen::MatrixXf y2Mat = box_center_yMat + box_heightsMat * 0.5;
      Eigen::MatrixXf x2Mat = box_center_xMat + box_widthsMat * 0.5;
      y1Mat.transposeInPlace();
      x1Mat.transposeInPlace();
      y2Mat.transposeInPlace();
      x2Mat.transposeInPlace();
      Eigen::RowVectorXf y1MatFlat(Eigen::Map<Eigen::VectorXf>(
          y1Mat.data(), y1Mat.rows() * y1Mat.cols()));
      Eigen::RowVectorXf x1MatFlat(Eigen::Map<Eigen::VectorXf>(
          x1Mat.data(), x1Mat.rows() * x1Mat.cols()));
      Eigen::RowVectorXf y2MatFlat(Eigen::Map<Eigen::VectorXf>(
          y2Mat.data(), y2Mat.rows() * y2Mat.cols()));
      Eigen::RowVectorXf x2MatFlat(Eigen::Map<Eigen::VectorXf>(
          x2Mat.data(), x2Mat.rows() * x2Mat.cols()));
      Eigen::MatrixXf boxes(y1Mat.rows() * y1Mat.cols(),
                            4); //注意这里的boxes不是python代码里面对应的boxes
      boxes.col(0) = y1MatFlat;
      boxes.col(1) = x1MatFlat;
      boxes.col(2) = y2MatFlat;
      boxes.col(3) = x2MatFlat;

      boxesVec.push_back(boxes);
      finalBoxesRows +=
          boxes.rows(); //统计五个RPN_ANCHOR_SCALES尺度对应的所有boxes的行数
                        // break;
    }
    //以上一步得到的boxes的finalBoxesRows为行数,4为列数创建二维矩阵finalBox(对应python代码的boxes),
    //其实就是用上面所有的boxes构建形式如[(y1, x1, y2, x2),...,...]的矩阵
    finalBox = Eigen::MatrixXf(finalBoxesRows, 4);
    int beginX = 0;
    for (size_t i = 0; i < boxesVec.size(); i++) {
      // mat1.block<rows,cols>(i,j)
      //矩阵块赋值
      finalBox.block(beginX, 0, boxesVec[i].rows(), boxesVec[i].cols()) =
          boxesVec[i];
      beginX += static_cast<int>(boxesVec[i].rows());
      // tensorflow::Tensor
      // matTensor(tensorflow::DT_FLOAT,{boxesVec[i].rows(),boxesVec[i].cols()});
    }

    //先创建scale,shift两个向量
    Eigen::MatrixXf scaleMat_1r(1, finalBox.cols());
    Eigen::MatrixXf shiftMat_1r(1, finalBox.cols());
    scaleMat_1r << float(inputImg_h - 1), float(inputImg_w - 1),
        float(inputImg_h - 1), float(inputImg_w - 1);
    shiftMat_1r << 0.f, 0.f, 1.f, 1.f;
    //因为上一步得到是scaleMat_1r,shiftMat_1r是向量,接下来创建对应的矩阵,该矩阵与finalBox有相同的
    //形状
    Eigen::MatrixXf scaleMat = scaleMat_1r.colwise().replicate(
        finalBox.rows()); //通过重复与finalBox同样的行数构建scaleMat
    Eigen::MatrixXf shiftMat =
        shiftMat_1r.colwise().replicate(finalBox.rows()); //同上
    Eigen::MatrixXf tmpMat =
        finalBox - shiftMat; // finalBox对应位置元素减去偏移量
    finalBox_norm =
        tmpMat.cwiseQuotient(scaleMat); // finalBox对应位置元素处以scale
    //至此完成了python代码中的boxes(finalBox_norm),下一步把finalBox_norm矩阵弄成Eigen::tensor类型的inputAnchorsTensor_temp
    //再通过inputAnchorsTensor_temp填充到tensorflow::tensor类型的inputAnchorsTensor构建最后送入模型的anchor
    //boxes

    inputAnchorsTensor =
        tensorflow::Tensor(tensorflow::DT_FLOAT,
                           {batch_size, finalBox_norm.rows(),
                            finalBox_norm.cols()}); //初始化inputAnchorsTensor
    // float *p=inputAnchorsTensor.flat<float>().data();
    //通finalBox_norm矩阵构建Eigen::tensor类型的inputAnchorsTensor_temp
    // Eigen::Tensor<float,3>inputAnchorsTensor_temp(1,finalBox_norm.rows(),finalBox_norm.cols());
    // for(int i=0;i<finalBox_norm.rows();i++){

    //     Eigen::Tensor<float,1>eachrow(finalBox_norm.cols());//用于临时存储finalBox_norm矩阵的的每一行
    //     //把finalBox_norm矩阵的一行放进eachrow
    //     eachrow.setValues({finalBox_norm.row(i)[0],finalBox_norm.row(i)[1],finalBox_norm.row(i)[2],finalBox_norm.row(i)[3]});
    //     //把eachrow放进inputAnchorsTensor_temp的每一行
    //     inputAnchorsTensor_temp.chip(i,1)=eachrow;
    // }
    // //把inputAnchorsTensor_temp赋值给inputAnchorsTensor,注意它们两个的类型是不同的
    // auto showMap=inputAnchorsTensor.tensor<float,3>();
    // for(int b=0;b<showMap.dimension(0);b++)
    // {
    //     for(int r=0;r<showMap.dimension(1);r++)
    //     {
    //         for(int c=0;c<showMap.dimension(2);c++)
    //         {

    //             showMap(b,r,c)=inputAnchorsTensor_temp(0,r,c);//这里为0是因为
    //             //我的batch里面的图片都是同样尺寸的,所以它们最终的anchor
    //             boxes都是一样,
    //             //只要赋值一个就行了,建议batch里面图片尺寸都是一样的,这样好处理
    //         }
    //     }
    // }
  }

  return inputAnchorsTensor;
}

cv::Mat TensorflowUtilities::predict(const cv::Mat &inputMat) {

  int TF_MASKRCNN_IMG_WIDTHHEIGHT = 1024;
  cv::Scalar TF_MASKRCNN_MEAN_PIXEL(123.7, 116.8, 103.9);
  float TF_MASKRCNN_IMAGE_METADATA[16] = {
      0,
      static_cast<float>(TF_MASKRCNN_IMG_WIDTHHEIGHT),
      static_cast<float>(TF_MASKRCNN_IMG_WIDTHHEIGHT),
      3,
      0,
      0,
      static_cast<float>(TF_MASKRCNN_IMG_WIDTHHEIGHT),
      static_cast<float>(TF_MASKRCNN_IMG_WIDTHHEIGHT),
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0};

  // Resize to square with max dim, so we can resize it to 1024*1024
  int largestDim = inputMat.size().height > inputMat.size().width
                       ? inputMat.size().height
                       : inputMat.size().width;
  cv::Mat squareInputMat(cv::Size(largestDim, largestDim), CV_8UC3);
  int leftBorder = (largestDim - inputMat.size().width) / 2;
  int topBorder = (largestDim - inputMat.size().height) / 2;
  cv::copyMakeBorder(inputMat, squareInputMat, topBorder,
                     largestDim - (inputMat.size().height + topBorder),
                     leftBorder,
                     largestDim - (inputMat.size().width + leftBorder),
                     cv::BORDER_CONSTANT, cv::Scalar(0));
  cv::Mat resizedInputMat(
      cv::Size(TF_MASKRCNN_IMG_WIDTHHEIGHT, TF_MASKRCNN_IMG_WIDTHHEIGHT),
      CV_8UC3);
  cv::resize(squareInputMat, resizedInputMat, resizedInputMat.size(), 0, 0);

  // Need to "mold_image" like in mask rcnn
  cv::Mat moldedInput(resizedInputMat.size(), CV_32FC3);
  resizedInputMat.convertTo(moldedInput, CV_32FC3);
  cv::subtract(moldedInput, TF_MASKRCNN_MEAN_PIXEL, moldedInput);

  // Move the data into the input tensor
  // remove memory copies by using code at
  // https://github.com/tensorflow/tensorflow/issues/8033#issuecomment-332029092
  // allocate a Tensor and get pointer to memory for that Tensor, allocate a
  // "fake" cv::Mat from it to use as a  basis to convert
  tensorflow::Tensor inputTensor(tensorflow::DT_FLOAT,
                                 {1, moldedInput.size().height,
                                  moldedInput.size().width,
                                  3}); // single image instance with 3 channels
  float_t *p = inputTensor.flat<float_t>().data();
  cv::Mat inputTensorMat(moldedInput.size(), CV_32FC3, p);
  moldedInput.convertTo(inputTensorMat, CV_32FC3);

  int TF_MASKRCNN_IMAGE_METADATA_LENGTH = 16;
  // Copy the TF_MASKRCNN_IMAGE_METADATA data into a tensor
  tensorflow::Tensor inputMetadataTensor(
      tensorflow::DT_FLOAT, {1, TF_MASKRCNN_IMAGE_METADATA_LENGTH});
  auto inputMetadataTensorMap = inputMetadataTensor.tensor<float, 2>();
  for (int i = 0; i < TF_MASKRCNN_IMAGE_METADATA_LENGTH; ++i) {
    inputMetadataTensorMap(0, i) = TF_MASKRCNN_IMAGE_METADATA[i];
  }

  tensorflow::SessionOptions opts;
  tensorflow::Session *session; // tensorflow'ssession
  tensorflow::GraphDef graphdef;

  std::string model_path = "/tmp/project/test/mask_rcnn_theater.pb";
  std::cout << "model path is !!!!!!!!! " << model_path << std::endl;
  tensorflow::Status status = NewSession(opts, &session);
  tensorflow::Status status_load =
      ReadBinaryProto(tensorflow::Env::Default(), model_path,
                      &graphdef); // read model from pb file

  if (!status_load.ok()) {
    std::cout << "Error: loading model failed !" << model_path << std::endl;
    std::cout << status_load.ToString() << std::endl;
  }
  tensorflow::Status status_create =
      session->Create(graphdef); // import model to session

  if (!status_create.ok()) {
    std::cout << "Error: Creating graph in session failed !"
              << status_create.ToString() << std::endl;
  }
  std::cout << "<-----Successfully created session and load graph---->"
            << std::endl;

  tensorflow::Tensor input_anchors = get_anchors();

  // Run tensorflow
  cv::TickMeter tm;
  tm.start();
  std::vector<tensorflow::Tensor> outputs;

  tensorflow::Status run_status =
      session->Run({{"input_image", inputTensor},
                    {"input_image_meta", inputMetadataTensor},
                    {"input_anchors", input_anchors}},
                   {"output_1", "output_2", "output_3", "output_4", "output_5",
                    "output_6", "output_7"},
                   {}, &outputs);
  if (!run_status.ok()) {
    std::cerr << "tfSession->Run failed: " << run_status << std::endl;
  }
  tm.stop();
  std::cout << "Inference time, ms: " << tm.getTimeMilli() << std::endl;

  if (outputs[3].shape().dims() != 5 || outputs[3].shape().dim_size(4) != 4) {
    throw std::runtime_error(
        "Expected mask dimensions to be [1,100,28,28,2] but got: " +
        outputs[3].shape().DebugString());
  }

  // auto detectionsMap = outputs[0].tensor<float, 3>();

  return moldedInput;
}

} // namespace rbc